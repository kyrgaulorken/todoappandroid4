package com.ork.android4.adapter

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.ListFragment
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.ork.android4.R
import com.ork.android4.data.TodoItem
import com.ork.android4.fragments.TodoItemListFragment
import com.ork.android4.fragments.TodoItemListFragmentDirections
import com.ork.android4.fragments.UpdateFragment
import kotlinx.android.synthetic.main.todo_item_layout.view.*

class TodoItemAdapter(
    private var todoItemList : List<TodoItem> = listOf(),
) : RecyclerView.Adapter<TodoItemAdapter.TodoItemViewHolder>() {

    inner class TodoItemViewHolder(private val view: View) : RecyclerView.ViewHolder(view) {
        fun bindTodoItem(todoItem : TodoItem) {
            view.todoTitle.text = todoItem.todoTitle.toString()
            view.todoDescription.text = todoItem.todoDescription.toString()
            view.todoDate.text = todoItem.todoDate.toString()
            when(todoItem.todoStatus.toLowerCase()) {
                "low" -> {
                    view.todoStatus.setBackgroundColor(Color.RED)
                }
                "medium" -> {
                    view.todoStatus.setBackgroundColor(Color.YELLOW)
                }
                "high" -> {
                    view.todoStatus.setBackgroundColor(Color.GREEN)

                }
            }
            view.todoItemElement.setOnClickListener {
                //val action = TodoItemListFragmentDirections.
                val action = TodoItemListFragmentDirections.actionTodoItemListFragmentToUpdateFragment(todoItem)
                view.findNavController().navigate(action)
            }
        }
    }

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): TodoItemViewHolder {
        return TodoItemViewHolder(
            LayoutInflater.from(p0.context).inflate(R.layout.todo_item_layout, p0, false)
        )
    }

    override fun onBindViewHolder(p0: TodoItemViewHolder, p1: Int) {
        p0.bindTodoItem(todoItemList[p1])
    }

    fun setTodoList(todoList : MutableList<TodoItem>) {
        this.todoItemList = todoList
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return todoItemList.size
    }

}