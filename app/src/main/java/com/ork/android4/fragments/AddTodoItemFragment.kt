package com.ork.android4.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.fragment.findNavController
import com.ork.android4.R
import com.ork.android4.data.TodoItem
import com.ork.android4.data.TodoItemViewModel
import kotlinx.android.synthetic.main.fragment_add_todo_item.*
import kotlinx.coroutines.InternalCoroutinesApi


class AddTodoItemFragment : Fragment() {


    @InternalCoroutinesApi
    private val viewModel: TodoItemViewModel by lazy {
        ViewModelProvider(this).get(TodoItemViewModel::class.java)
    }

    @InternalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_add_todo_item, container, false)
    }


    @InternalCoroutinesApi
    override fun onResume() {
        super.onResume()
        //todoItemViewModel  =  ViewModelProvider(this).get(TodoItemViewModel::class.java)
        addTodoItemBtn.setOnClickListener {
            insertTodoItem()
        }
    }

    @InternalCoroutinesApi
    fun insertTodoItem() {
        val title = itemNameEdit.text.toString().trim()
        val description = itemDescEdit.text.toString().trim()
        val date = itemDateEdit.text.toString().trim()
        var status = ""
        when (radioGroupStatus.checkedRadioButtonId) {
            R.id.lowRadio -> {
                status = lowRadio.text.toString()
            }
            R.id.mediumRadio -> {
                status = mediumRadio.text.toString()
            }
            R.id.highRadio -> {
                status = highRadio.text.toString()
            }
        }
        if(validation(title, description, date, status)) {
            val todoItem = TodoItem(
                0,
                todoTitle = title,
                todoDescription = description,
                todoDate = date,
                todoStatus = status
            )
            viewModel.addTodoItem(todoItem)
            Toast.makeText(requireContext(), "Success", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_addTodoItemFragment_to_todoItemListFragment)
        }
    }


    private fun validation(
        title: String,
        description: String,
        date: String,
        status: String,

        ): Boolean {
        if (title.isNotEmpty() && description.isNotEmpty() && date.isNotEmpty() && status.isNotEmpty()) {
            return true
        }
        return false
    }
}