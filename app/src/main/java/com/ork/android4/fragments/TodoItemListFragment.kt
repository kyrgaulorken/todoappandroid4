package com.ork.android4.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ork.android4.R
import com.ork.android4.adapter.TodoItemAdapter
import com.ork.android4.data.TodoItem
import com.ork.android4.data.TodoItemViewModel
import kotlinx.android.synthetic.main.fragment_todo_item_list.*
import kotlinx.coroutines.InternalCoroutinesApi

class TodoItemListFragment : Fragment() {

    @InternalCoroutinesApi
    private val viewModel: TodoItemViewModel by lazy {
        ViewModelProvider(this).get(TodoItemViewModel::class.java)
    }

    @InternalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        requireActivity().actionBar?.setTitle("Список задач")


        //todoItemList.layoutManager = LinearLayoutManager(requireContext())
        viewModel.readAllData.observe(viewLifecycleOwner, {
           if(it != null) {
               //Log.d("listOfData",it.toString())
              todoItemList.adapter = TodoItemAdapter(it)
               todoItemList.layoutManager = LinearLayoutManager(requireContext())
           }
        })
        setHasOptionsMenu(true)
         return  inflater.inflate(R.layout.fragment_todo_item_list, container, false)
    }

    override fun onResume() {
        super.onResume()
        addTodoItemBtnList.setOnClickListener {
            findNavController().navigate(R.id.action_todoItemListFragment_to_addTodoItemFragment)
        }
    }


    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        return inflater.inflate(R.menu.menu_delete,menu)
    }

    @InternalCoroutinesApi
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.delete_todoItem) {
            deleteAllTodoItem()
        }
        return super.onOptionsItemSelected(item)
    }


    @InternalCoroutinesApi
    fun deleteAllTodoItem() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Удаление")
            .setMessage("Вы точно хотите удалить весь список?")
            .setNegativeButton("нет") { dialog, which ->
                dialog.dismiss()
            }
            .setPositiveButton("да") { dialog, which ->
                viewModel.deleteAllTodoItem()
//                findNavController().navigate(R.id.action_updateFragment_to_todoItemListFragment)
            }
            .show()
    }

}