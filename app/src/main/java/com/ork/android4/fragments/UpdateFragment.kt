package com.ork.android4.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.ork.android4.R
import com.ork.android4.data.TodoItem
import com.ork.android4.data.TodoItemViewModel
import com.ork.android4.databinding.FragmentUpdateBinding
import kotlinx.android.synthetic.main.fragment_add_todo_item.*
import kotlinx.android.synthetic.main.fragment_update.*
import kotlinx.coroutines.InternalCoroutinesApi

class UpdateFragment : Fragment() {

    private val arguments by navArgs<UpdateFragmentArgs>()
    @InternalCoroutinesApi
    private lateinit var viewModel : TodoItemViewModel

    //private lateinit var binding : FragmentUpdateBinding

    @InternalCoroutinesApi
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        viewModel = ViewModelProvider(this).get(TodoItemViewModel::class.java)
        //binding = FragmentUpdateBinding.inflate(layoutInflater)

        //Log.d("todoObject", arguments.currentTodoItem.toString())
        //itemNameEditUpdate.setText(arguments.currentTodoItem.todoTitle)
        setHasOptionsMenu(true)
        return inflater.inflate(R.layout.fragment_update, container, false)
    }

    @InternalCoroutinesApi
    override fun onResume() {
        super.onResume()
        print(arguments.currentTodoItem.toString())
        //Log.d("todoObject", arguments.currentTodoItem.toString())
        if(arguments.currentTodoItem != null){
            itemNameEditUpdate.setText(arguments.currentTodoItem.todoTitle.toString()).toString()
            itemDescEditUpdate.setText(arguments.currentTodoItem.todoDescription).toString()
            itemDateEditUpdate.setText(arguments.currentTodoItem.todoDate).toString()
            //Log.d("status",arguments.currentTodoItem.todoStatus.toString())
            //radioGroupStatusUpdate.check(R.id.lowRadio)
            when(arguments.currentTodoItem.todoStatus.toLowerCase()) {
                "low" -> {
                    lowRadioUpdate.isChecked = true
                }
                "medium" -> {
                   mediumRadioUpdate.isChecked = true
                }
                "high" -> {
                    highRadioUpdate.isChecked = true
                }
            }
        }


        updateTodoItemBtnUpdate.setOnClickListener {
            updateTodoItem()
        }
    }


    @InternalCoroutinesApi
    private fun updateTodoItem() {
        val todoTitle = itemNameEditUpdate.text.toString().trim()
        val todoDescription = itemDescEditUpdate.text.toString().trim()
        val todoDate = itemDateEditUpdate.text.toString().trim()
        var status = ""
        //Toast.makeText(requireContext(), status.toString(), Toast.LENGTH_SHORT).show()
        when (radioGroupStatusUpdate.checkedRadioButtonId) {
            R.id.lowRadioUpdate -> {
                status = lowRadioUpdate.text.toString()
            }
            R.id.mediumRadioUpdate -> {
                status = mediumRadioUpdate.text.toString()
            }
            R.id.highRadioUpdate -> {
                status = highRadioUpdate.text.toString()
            }
        }

        val todoItem = TodoItem(
            arguments.currentTodoItem.id,
            todoTitle = todoTitle,
            todoDescription = todoDescription,
            todoDate = todoDate,
            todoStatus = status
        )
        //Toast.makeText(requireContext(), todoItem.toString(), Toast.LENGTH_SHORT).show()
//        print(status)
//        Log.d("status",status)
        if(validation(todoTitle, todoDescription, todoDate, status)) {
            val todoItem = TodoItem(
                arguments.currentTodoItem.id,
                todoTitle = todoTitle,
                todoDescription = todoDescription,
                todoDate = todoDate,
                todoStatus = status
            )
            viewModel.updateTodoItem(todoItem = todoItem)
            Toast.makeText(requireContext(), "updated", Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_updateFragment_to_todoItemListFragment)
        }
    }

    private fun validation(
        title: String,
        description: String,
        date: String,
        status: String,

        ): Boolean {
        if (title.isNotEmpty() && description.isNotEmpty() && date.isNotEmpty() && status.isNotEmpty()) {
            return true
        }
        return false
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        return inflater.inflate(R.menu.menu_delete,menu)
    }

    @InternalCoroutinesApi
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if(item.itemId == R.id.delete_todoItem) {
            deleteTodoItem()
        }
        return super.onOptionsItemSelected(item)
    }

    @InternalCoroutinesApi
    fun deleteTodoItem() {
        MaterialAlertDialogBuilder(requireContext())
            .setTitle("Удаление")
            .setMessage("Вы точно хотите удалить ${arguments.currentTodoItem.todoTitle}?")
            .setNegativeButton("нет") { dialog, which ->
                dialog.dismiss()
            }
            .setPositiveButton("да") { dialog, which ->
                viewModel.deleteTodoItem(arguments.currentTodoItem)
                findNavController().navigate(R.id.action_updateFragment_to_todoItemListFragment)
            }
            .show()
    }


}