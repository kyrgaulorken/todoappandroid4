package com.ork.android4.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.launch

@InternalCoroutinesApi
class TodoItemViewModel(application: Application): AndroidViewModel(application) {

    val readAllData: LiveData<List<TodoItem>>
    private val repository: TodoItemRepo

    init {
        val userDao = TodoItemDatabase.getInstance(
            application
        ).todoDao()
        repository = TodoItemRepo(userDao)
        readAllData = repository.getAllTodoItems
    }

    fun addTodoItem(todoItem: TodoItem){
        viewModelScope.launch(Dispatchers.IO) {
            repository.addTodoItem(todoItem)
        }
    }

    fun updateTodoItem(todoItem: TodoItem){
        viewModelScope.launch(Dispatchers.IO) {
            repository.updateTodoItem(todoItem)
        }
    }

    fun deleteTodoItem(todoItem: TodoItem){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteTodoItem(todoItem)
        }
    }

    fun deleteAllTodoItem(){
        viewModelScope.launch(Dispatchers.IO) {
            repository.deleteAllTodoItem()
        }
    }

}