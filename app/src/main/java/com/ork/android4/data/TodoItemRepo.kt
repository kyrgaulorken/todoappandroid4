package com.ork.android4.data

import androidx.lifecycle.LiveData

class TodoItemRepo(private val todoItemDao: TodoItemDao) {

    val getAllTodoItems: LiveData<List<TodoItem>> = todoItemDao.getAllTodoItems()

    suspend fun addTodoItem(todoItem: TodoItem) {
        todoItemDao.addTodoItem(todoItem)
    }

    suspend fun updateTodoItem(todoItem: TodoItem) {
        todoItemDao.updateTodoItem(todoItem)
    }

    suspend fun deleteTodoItem(todoItem: TodoItem) {
        todoItemDao.deleteTodoItem(todoItem)
    }

    suspend fun deleteAllTodoItem() {
        todoItemDao.deleteAllTodoItems()
    }

}