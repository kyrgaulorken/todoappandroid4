package com.ork.android4.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import kotlinx.coroutines.InternalCoroutinesApi
import kotlinx.coroutines.internal.synchronized


@Database(entities = [TodoItem::class], version = 1,exportSchema = false)
abstract class TodoItemDatabase : RoomDatabase() {

    abstract fun todoDao(): TodoItemDao

    companion object {

        @Volatile
        private var INSTANCE: TodoItemDatabase? = null

        @InternalCoroutinesApi
        fun getInstance(context: Context): TodoItemDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room
                    .databaseBuilder(context.applicationContext, TodoItemDatabase::class.java, "todo_database")
                    .build()

                INSTANCE = instance
                return instance
            }

        }

    }

}