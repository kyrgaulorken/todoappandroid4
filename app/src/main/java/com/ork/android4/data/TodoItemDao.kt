package com.ork.android4.data

import androidx.lifecycle.LiveData
import androidx.room.*


@Dao
interface TodoItemDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTodoItem(todoItem: TodoItem)

    @Query("SELECT * FROM todoitem ORDER BY id ASC")
    fun getAllTodoItems() : LiveData<List<TodoItem>>

    @Update
    suspend fun updateTodoItem(todoItem: TodoItem)

    @Delete
    suspend fun deleteTodoItem(todoItem: TodoItem)

    @Query("DELETE FROM todoitem")
    suspend fun deleteAllTodoItems()

}
