package com.ork.android4.data

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize


@Entity
@Parcelize
data class TodoItem(
    @PrimaryKey(autoGenerate = true)
    val id : Int,
    val todoTitle : String,
    val todoDescription : String,
    val todoDate : String,
    val todoStatus : String
) : Parcelable